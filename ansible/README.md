# sett benchmarks

## Description

Basically, the playbook executes the following roles in this order:
- `client` - sets up the VM by adding missing executables like **sett** and **minio** client
- `services` - deploys **minio** and **sftp** services
- `data` - prepares the data for the benchmarks
- `benchmarks` - runs the benchmarks

At the end, results are uploaded to the **S3** bucket defined in `s3_benchmarks_bucket` (by default _sett-benchmarks_).

## Prerequisites

You will need a **Python** environment with `ansible` installed: `pip install ansible --upgrade`.
When `ansible` is available, trigger a `ansible-galaxy install -r requirements.yml` to fetch the playbook dependencies.

## Local Vagrant deployment

If you wish to test a deployment locally, or to test changes to `playbook.yml`, you can do so using **Vagrant**:

Requirements for host machine:

1. Install **Vagrant**. For details, see [instructions](https://learn.hashicorp.com/tutorials/vagrant/getting-started-install?in=vagrant/getting-started) on the website.
2. Install [**VirtualBox**](https://www.virtualbox.org/)
3. The `ansible` command must be available in your PATH.
4. Install ansible playbook dependencies `ansible-galaxy install -r requirements.yml`
5. From the project root directory, run `vagrant up`.

This will create and launch a virtual machine that right after startup will run the playbook. Run `vagrant halt` to stop the virtual machine, or `vagrant destroy` to destroy it.

You can test the local **MinIO** deployment at your host using http://localhost:9001 to access the console. You should be able to login using the service credentials defined in `group_vars/vagrant.yml`.

When the playbook is finished, results are published in the `benchmarks` bucket.

## Remote (AWS)

```bash
ansible-playbook -i inventory/hosts.yml -l remote --skip-tags "sftp"
```
If you want to use a different **S3** endpoint, then adapt the group variables in `group_vars/remote.yml` accordingly. We do not benchmark **SFTP** as the component is not deployed on **AWS**. Currently, **SFTP** benchmarks are only available for the `local` deployment.

For targeting **AWS S3** repository, do NOT set `s3_endpoint`.

## Tags

It is possible to run only a subset of the playbook using tags. For example, you could skip all the SFTP benchmarks by running the following command:

```bash
ansible-playbook -i inventory/hosts.yml -l remote --skip-tags "sftp"
```

Or just upload the results to the **S3** bucket:

```bash
ansible-playbook -i inventory/hosts.yml -l remote --tags "results"
```
