#!/usr/bin/env bash

set -e # exit on first error

for dir_name in data benchmarks results; do
  mkdir -p $dir_name
done
rm -rf data/*

LOCAL_ENCRYPT_FILE_NAME=local-encrypt.zip
FINGERPRINT=865079A1B73BA3F11ED0BDF7A5A9FEB57294CC4D
SETT=/Users/christianr/Downloads/sett-cli_5.2.0_aarch64-apple-darwin

for DATA_SIZE_IN_MB in 20 300 1000; do
  dd if=/dev/urandom of=data/data.bin bs=1M count=${DATA_SIZE_IN_MB}
  # local encrypt
  hyperfine --export-json benchmarks/local-encrypt-${DATA_SIZE_IN_MB}mb.json -w 3 -r 5 "SETT_OPENPGP_KEY_PWD=benchmarks ${SETT} encrypt local -q -s ${FINGERPRINT} -r ${FINGERPRINT} -o ${LOCAL_ENCRYPT_FILE_NAME} data >> benchmarks/local-encrypt-output-${DATA_SIZE_IN_MB}mb.txt 2>&1"
  # s3 transfer
  hyperfine --export-json benchmarks/s3-transfer-${DATA_SIZE_IN_MB}mb.json -w 3 -r 5 "${SETT} transfer s3 -q -e http://localhost:9000/ --access-key minioadmin --secret-key minioadmin -b transfers ${LOCAL_ENCRYPT_FILE_NAME} >> benchmarks/s3-transfer-output-${DATA_SIZE_IN_MB}mb.txt 2>&1"
  # sftp transfer
  hyperfine --export-json benchmarks/sftp-transfer-${DATA_SIZE_IN_MB}mb.json -w 3 -r 5 "${SETT} transfer sftp -q -H localhost -u sftp --key-path ~/.ssh/sftp_ssh_testkey --base-path upload ${LOCAL_ENCRYPT_FILE_NAME} >> benchmarks/sftp-transfer-output-${DATA_SIZE_IN_MB}mb.txt 2>&1"
  # s3 encrypt
  hyperfine --export-json benchmarks/s3-encrypt-${DATA_SIZE_IN_MB}mb.json -w 3 -r 5 "SETT_OPENPGP_KEY_PWD=benchmarks ${SETT} encrypt s3 -q -e http://localhost:9000/ --access-key minioadmin --secret-key minioadmin -b transfers -s ${FINGERPRINT} -r ${FINGERPRINT} data >> benchmarks/s3-encrypt-output-${DATA_SIZE_IN_MB}mb.txt 2>&1"
  # sftp encrypt
  hyperfine --export-json benchmarks/sftp-encrypt-${DATA_SIZE_IN_MB}mb.json -w 3 -r 5 "SETT_OPENPGP_KEY_PWD=benchmarks ${SETT} encrypt sftp -q -H localhost -u sftp --key-path ~/.ssh/sftp_ssh_testkey --base-path upload -s ${FINGERPRINT} -r ${FINGERPRINT} data >> benchmarks/sftp-encrypt-output-${DATA_SIZE_IN_MB}mb.txt 2>&1"
  # local decrypt
  hyperfine --export-json benchmarks/local-decrypt-${DATA_SIZE_IN_MB}mb.json -w 3 -r 5 "SETT_OPENPGP_KEY_PWD=benchmarks ${SETT} decrypt local -q local-encrypt.zip >> benchmarks/local-decrypt-output-${DATA_SIZE_IN_MB}mb.txt 2>&1"
  # s3 decrypt
  hyperfine --export-json benchmarks/s3-decrypt-${DATA_SIZE_IN_MB}mb.json -w 3 -r 5 "SETT_OPENPGP_KEY_PWD=benchmarks ${SETT} decrypt s3 -q -e http://localhost:9000/ --access-key minioadmin --secret-key minioadmin -b transfers ${LOCAL_ENCRYPT_FILE_NAME} >> benchmarks/s3-decrypt-output-${DATA_SIZE_IN_MB}mb.txt 2>&1"
done

rm -f $LOCAL_ENCRYPT_FILE_NAME
rm -rf local-encrypt*
