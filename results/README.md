# Benchmark Results

This folder contains the results of the benchmarks, grouped by **sett** version. To generate the plots, run the following command:

```bash
./run.py <version>
```

Following structure below each version is expected to have the script working properly:
- `local`: Contains the results of the benchmarks performed locally.
- `aws`: Contains the results of the benchmarks performed on **AWS**. No **minio** instance is involved. **AWS** **S3** repository is used instead.
- `biomedit`: **S3** transfers are performed between **AWS** and **BioMedIT** network where a **minio** instance is deployed. 
