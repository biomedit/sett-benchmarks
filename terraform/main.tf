terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
  required_version = ">= 1.7.5"
}

provider "aws" {
  profile = var.aws_profile
  region  = var.aws_region
}

data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-arm64-server-*"]
  }
  owners = ["amazon"]
}

# Create an EC2 instance
resource "aws_instance" "sett-benchmarks" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  key_name = var.key_name

  ebs_block_device {
    device_name = "/dev/sda1"
    volume_size = var.volume_size
    delete_on_termination = true
  }

}

# S3
resource "aws_s3_bucket" "buckets" {
  count         = length(var.aws_buckets)
  bucket        = var.aws_buckets[count.index]
  force_destroy = true
}
