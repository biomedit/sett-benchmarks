output "ec2_hostname" {
  description = "The public DNS to access the EC2 instance. This value has to be set in the Ansible inventory file for the benchmarks."
  value = aws_instance.sett-benchmarks.public_dns
}

